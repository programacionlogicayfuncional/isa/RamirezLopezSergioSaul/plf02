(ns plf02.core)

;función associative?
(defn función-associative?-1
  [xs]
  (associative? xs))

(defn función-associative?-2
  [x]
  (associative? x))

(defn función-associative?-3
  [a]
  (associative? a))

(función-associative?-1 [1 2 3 5])
(función-associative?-2 '(1 2 3  5))
(función-associative?-3 {:a 1 :b 2 :c 4 :d 6})

;función boolean?
(defn función-boolean?-1
  [x]
  (boolean? x))

(defn función-boolean?-2
  [xs]
  (boolean? xs))

(defn función-boolean?-3
  [ xs]
  (boolean? xs))

(función-boolean?-1 false)
(función-boolean?-2 (first [1 3 true "s"]))
(función-boolean?-3 (max [29 12 23]))

;función char?
(defn función-char?-1
  [x]
  (char? x))

(defn función-char?-2
  [xs]
  (char? xs))

(defn función-char?-3
  [a]
  (char?  a))

(función-char?-1 \a)
(función-char?-2 (first [\c 1 \b 23 "s"]))
(función-char?-3 (first [\b 2 3 'd' "s"]))

;función coll?
(defn función-coll?-1
  [xs]
  (coll? xs))

(defn función-coll?-2
  [xs]
  (coll? xs))

(defn función-coll?-3
  [x]
  (coll? x))

(función-coll?-1 '(1 2 3))
(función-coll?-2 (first [[1 2 3] ["s" "f" "g"]]))
(función-coll?-3 12)
(función-coll?-3 "sergio")

;función decimal?
(defn función-decimal?-1
  [a]
  (decimal? a))

(defn función-decimal?-2
  [xs]
  (decimal? xs))

(defn función-decimal?-3
  [x]
  (decimal? x))

(función-decimal?-1 1M)
(función-boolean?-2 (first #{999999999999999999999}))
(función-decimal?-3 (first {1 3 4 true false 4 "s" 3}))

;función double?
(defn función-deuble?-1
  [a]
  (double? a))

(defn función-deuble?-2
  [x]
  (double? x))

(defn función-deuble?-3
  [xs]
  (double? xs))

(función-deuble?-1 1)
(función-deuble?-2 (new Double "100"))
(función-deuble?-3 (first [10.2 10 20 1.1]))

;función float
(defn función-float?-1
  [x]
  (float? x))

(defn función-float?-2
  [xs]
  (float? xs))

(defn función-float?-3
  [a]
  (float? a))

(función-float?-1 1.0)
(función-float?-2 (first [true false 1.10 "a"]))
(función-float?-3 10M)

;función ident?
(defn función-ident?-1
  [a]
  (ident? a))

(defn función-ident?-2
  [a xs]
  (ident? (a xs)))

(defn función-ident?-3
  [f xs]
  (ident? (f xs)))

(función-ident?-1 :hello)
(función-ident?-2 first ["good" "Day"])
(función-ident?-3 vals {:a 3 :b 5})

;funcion indexed?
(defn función-idexed?-1
  [xs]
  (indexed? xs))

(defn función-idexed?-2
  [a xs]
  (indexed? (a xs)))

(función-idexed?-1 [1 3 4 6])
(función-idexed?-2 first [5 6 7 8])
(función-idexed?-1 (first #{"a" "b" "d"}))

;función int?
(defn función-int?-1
  [a]
  (int? a))

(defn función-int?-2
  [x]
  (int? (java.lang.Long. x)))

(defn función-int?-3
  [xs]
  (int? xs))

(función-int?-1 2.0)
(función-int?-2 42)
(función-int?-3 (first [true 12 1.0 "ss" \a]))

;funcion integer?
(defn función-integer?-1
  [x]
  (integer? x))

(defn función-integer?-2
  [a ax bx]
  (integer? (a ax bx)))

(defn función-integer?-3
  [a b]
  (integer? (a b)))

(función-integer?-1 true)
(función-integer?-2 * (first [4 5 6]) (first [3 4 2]))
(función-integer?-3 inc 32)

;función keyword?
(defn función-keyword?-1
  [x]
  (keyword? x))

(defn función-keyword?-2
  [xs v]
  (keyword? (v xs)))

(defn función-keyword?-3
  [xs]
  (keyword? xs))

(función-keyword?-1 :a)
(función-keyword?-2 {:a 4 :b 5} keys)
(función-keyword?-3 (first [2 3 4 5]))

;función list?
(defn función-list?-1
  [x]
  (list? x))

(defn función-list?-2
  [xs]
  (list? xs))

(defn función-list?-3
  [n]
  (list? n))

(función-list?-1 10)
(función-list?-1 [1 2 3 5])
(función-list?-2 '())
(función-list?-3 '(range 3))

;función map
(defn función-map?-1
  [xs]
  (map? xs))

(defn función-map?-2
  [xs]
  (map? xs))

(función-map?-1 '(1 2 3))
(función-map?-1 {:a 1 :b 2 :c 3})
(función-map?-2 (hash-map :a 3 :b 5))

;función nat-int?
(defn función-nat-int?-1
  [a]
  (nat-int? a))

(defn función-nat-int?-2
  [a xs]
  (nat-int? (a xs)))

(defn función-nat-int?-3
  [x]
  (nat-int? x))

(función-nat-int?-1 -1)
(función-nat-int?-2 first [-10 -5 0 13 19 3 2])
(función-nat-int?-3 (first (range 4 10 2)))

;función pos-int?
(defn función-pos-int?-1
  [a]
  (pos-int? a))

(defn función-pos-int?-2
  [xs]
  (pos-int? xs))

(defn función-pos-int?-3
  [a b c]
  (pos-int? (c a b)))

(función-pos-int?-1 92233720368547)
(función-pos-int?-2 (first [10 20 40 50 112]))
(función-pos-int?-3 10 30 /)

;función number?
(defn función-number?-1
  [x]
  (number? x))

(defn función-number?-2
  [xs]
  (number? xs))

(defn función-number?-3
  [a xs bx]
  (number? (a xs bx)))

(función-number?-1 10)
(función-number?-1 true)
(función-number?-2 (first (range 5)))
(función-number?-3 * (first [10 23]) (first [22 3]))

;función ratio?
(defn función-radio?-1
  [x]
  (ratio? x))

(defn función-radio?-2
  [a xs bx]
  (ratio? (a xs bx)))

(defn función-radio?-3
  [a b c]
  (ratio? (c a b)))

(función-radio?-1 10)
(función-radio?-2 + (first [10 20 30]) (first [20 true false]))
(función-radio?-3 22 7 /)

;función rational?)
(defn función-rational-1
  [n]
  (rational? n))

(defn función-rational-2
  [a b c]
  (rational? (c a b)))

(defn función-rational-3
  [a xs bx]
  (rational? (a xs bx)))

(función-rational-1 12)
(función-rational-2 10 1.5 -)
(función-rational-3 / (first [10 20 40]) (first [22 3 4]))

;función seq
(defn función-seq-1
  [x]
  (seq? x))

(defn función-seq-2
  [n b c f]
  (seq? (f n b c)))

(defn función-seq-3
  [xs]
  (seq? xs))

(función-seq-1 '(1 2 3))
(función-seq-2 10 25 3 range)
(función-seq-3 {:a 2 :b 1})

;función sequential?
(defn función-sequential?-1
  [xs]
  (sequential? xs))

(defn función-sequential?-2
  [xs]
  (sequential? xs))

(defn función-sequential?-3
  [n]
  (sequential? n))

(función-sequential?-1 '(1 2 3 4 5))
(función-sequential?-2 (first [[5 10 15 20] [12 3 4 5]]))
(función-sequential?-3 (range -10 10))

;función set?
(defn función-set?-1
  [xs]
  (set? xs))

(defn función-set?-2
  [xs]
  (set? xs))

(defn función-set?-3
  [xs]
  (set? xs))

(función-set?-1 [1 2 3 5 6])
(función-set?-2 (hash-set [2 \a 3 \b 5 true 6 7 false 8 "asdd" "saul"]))
(función-set?-3 (sorted-set [1 4 6 7 9 10 20]))

;función seqable?
(defn función-seqable?-1
  [x]
  (seqable? x))

(defn función-seqable?-2
  [xs]
  (seqable? xs))

(defn función-seqable?-3
  [a]
  (seqable? a))

(función-seqable?-1 true)
(función-seqable?-2 {})
(función-seqable?-3 (int-array 5 1))

;función some?
(defn función-some?-1
  [x]
  (some? x))

(defn función-some?-2
  [xs]
  (some? xs))

(defn función-some?-3
  [xs]
  (some? xs))

(función-some?-1 49)
(función-some?-2 [nil])
(función-some?-3 (first [nil []]))

;función string?
(defn función-string?-1
  [a]
  (string? a))

(defn función-string?-2
  [xs]
  (string? xs))

(defn función-string?-3
  [a xs bx]
  (string? (a xs bx)))

(función-string?-1 "Saul")
(función-string?-2 (first [:s \a 12 "sw" :s 3 7]))
(función-string?-3 + (first [6 3 4]) (first [12 34]))

;función symbol
(defn función-symbol?-1
  [x]
  (symbol? x))

(defn función-symbol?-2
  [xs]
  (symbol? xs))

(función-symbol?-1 "a")
(función-symbol?-1 'a')
(función-symbol?-2 (first [1 3 true]))

;función vector?
(defn función-vector?-1
  [xs]
  (vector? xs))

(defn función-vector?-2
  [xs]
  (vector? xs))

(defn función-vector?-3
  [x y]
  (vector? (x y)))

(función-vector?-1 [1 2 3])
(función-vector?-2 (first {:a 1 :b 2 :c 3}))
(función-vector?-3 vector '(1 true false "ss" 12))

;función drop
(defn función-drop-1
  [x y]
  (drop x y))

(defn función-drop-2
  [a b]
  (drop a b))

(defn función-drop-3
  [a xs]
  (drop a xs))

(función-drop-1 5 (range 20))
(función-drop-2 (first [2 3 4]) (range 2 10))
(función-drop-3 4 (range -3 20 2))

;función drop-last
(defn función-drop-last-1
  [a]
  (drop-last a))

(defn función-drop-last-2
  [a xs]
  (drop-last (a xs) xs))

(defn función-drop-last-3
  [a ax]
  (drop-last a ax))

(función-drop-last-1 [1 2 3 4 9 1 5 7 8])
(función-drop-last-2 first (range 2 10))
(función-drop-last-3 2 {:a 1 :b 2 :c 3 :d 4 :e 5 :f 7})

;función drop-while
(defn función-drop-while-1
  [a b]
  (drop-while a b))

(defn función-drop-while-2
  [s]
  (drop-while s))

(defn función-drop-while-3
  [a xs]
  (drop-while a xs))

(función-drop-while-2 pos?)
(función-drop-while-1 neg? [-1 -2 -6 -7 1 2 3 4 -5 -6 0 1])
(función-drop-while-3 char? (range 2 10))

;función every?
(defn función-every?-1
  [a b]
  (every? a b))

(defn función-every?-2
  [x xs]
  (every? x xs))

(defn función-every?-3
  [xs bx]
  (every? xs bx))

(función-every?-1 even? '(2 4 6))
(función-every?-2 decimal? (first {2 3 4 6}))
(función-every?-3 #{1 2} [1 2])

;función filterv
(defn función-filterv-1
  [x]
  (filterv even? x))

(defn función-filterv-2
  [xs ys]
  (filterv xs ys))

(defn función-filterv-3
  [a b]
  (filterv a b))

(función-filterv-1 [2 4 5 6])
(función-filterv-2 even? (range 11))
(función-filterv-3 char? ["s" true false \d \j 2 4 'a' \a 6 "s" \v])

;función group-by
(defn función-group-by-1
  [a]
  (group-by count a))

(defn función-group-by-2
  [a b]
  (group-by a b))

(función-group-by-1 ["a" "as" "asd" "aa" "asdf" "qqqr"])
(función-group-by-2 odd? (range 1 11))
(función-group-by-2 number? [3 5 true false "s" \a \b \c 'd' 2 10])

;función iterate
(defn función-iterate-1
  [x]
  (iterate inc x))

(defn función-iterate-2
  [a b]
  (iterate a b))

(defn función-iterate-3
  [a b]
  (take a b))

(función-iterate-1 5)
(función-iterate-2 dec (first (range 2 6)))
(función-iterate-3 5 (iterate inc 3))

;funcion keep
(defn función-keep-1
  [a xs]
  (keep a xs))

(defn función-keep-2
  [a b]
  (keep a b))

(defn función-keep-3
  [x y]
  (keep x y))

(función-keep-1 even? [1 2 3 4 5 5 6 7 88 9 9 0])
(función-keep-2 pos? (range -6 10))
(función-keep-3 {:a 1, :b 2, :c 3 :h 6} [:a :b :d :e :g :h :y])

;función keep-indexed
(defn función-keep-idexed-1
  [a b]
  (keep-indexed a b))

(defn función-keep-idexed-2
  [xs bx]
  (keep-indexed xs bx))

(defn función-keep-idexed-3
  [xs a]
  (keep-indexed xs a))

(función-keep-idexed-1 #(if (odd? %1) %2) [:a :b :c :d :e])
(función-keep-idexed-2 #(if (pos? %2) %1) [-9 0 29 -7 45 3 -8])
(función-keep-idexed-3 #(if (pos? %2) %1) (range -3 6))

;función map-indexed
(defn función-map-indexed-1
  [x]
  (map-indexed (fn [idx itm] [idx itm]) x))

(defn función-map-indexed-2
  [a b]
  (map-indexed a b))

(defn función-map-indexed-3
  [x y]
  (map-indexed x y))

(función-map-indexed-1 "Hola Mundo")
(función-map-indexed-2 vector "Oaxaca")
(función-map-indexed-3 hash-map [\a \b \s "f" 's' 01])

;función mapcat
(defn función-mapcat-1
  [x]
  (mapcat reverse x))

(defn función-mapcat-2
  [a b]
  (mapcat a b))

(defn función-mapcat-3
  [x y]
  (mapcat x y))

(función-mapcat-1 ["saul"])
(función-mapcat-2 reverse [[3 2 1 0] [6 5 4] [9 8 7]])
(función-mapcat-3 reverse [[22 's' 42.2] [false true "sss"]])

;función mapv
(defn función-mapv-1
  [a b c]
  (mapv c a b))

(defn función-mapv-2
  [x y z]
  (mapv x y z))

(función-mapv-1 [1 2 3] [4 5 6] +)
(función-mapv-2 * (range 10) (range -10 10))
(función-mapv-2 + [1 2 3] (iterate inc 5))

;función merge-with
(defn función-merge-with-1
  [a xs xb]
  (merge-with a xs xb))

(defn función-merge-with-2
  [a b c]
  (merge-with a b c))

(función-merge-with-1 + {:a 1  :b 2} {:a 9  :b 98 :c 0})
(función-merge-with-2 into {"Lisp" ["Common Lisp" "Clojure"]
                            "ML" ["Caml" "Objective Caml"]}
                      {"Lisp" ["Scheme"]
                       "ML" ["Standard ML"]})
(función-merge-with-2 - {:a 5 :b 6 :d 1 :c 4} {:a 4 :d 3 :c 1})

;función not-any?
(defn función-not-any?-1
  [f xs]
  (not-any? f xs))

(defn función-not-any?-2
  [x y]
  (not-any? x y))

(función-not-any?-1 odd? [2 4 5 6])
(función-not-any?-2 odd? (range 19 2))
(función-not-any?-2 neg? [10 2 12])

;función not-every?
(defn función-not-every?-1
  [x y]
  (not-every? x y))

(defn función-not-every?-2
  [a xs]
  (not-every? a xs))

(defn función-not-every?-3
  [x y]
  (not-every? x y))

(función-not-every?-1 odd? [-2 3 5 1])
(función-not-every?-2 odd? (range 19 2))
(función-not-every?-3 even? (range -3 5))

;función partition-by
(defn función-partition-by-1
  [y x]
  (partition-by y x))

(defn función-partition-by-2
  [a b]
  (partition-by a b))

(defn función-partition-by-3
  [x y]
  (partition-by x y))

(función-partition-by-1 odd? [1 1 1 2 2 3 3])
(función-partition-by-2 identity "SsaaaaaaUuuuulllllllllll")
(función-partition-by-3 even? [1 1 1 2 2 3 3])

;función remove
(defn función-remove-1
  [x]
  (remove pos? x))

(defn función-remove-2
  [a b]
  (remove a b))

(defn función-remove-3
  [a xs]
  (remove a xs))

(función-remove-1 [1 -2 2 -1 3 7 0 -4 -7 6])
(función-remove-2 nil? [1 nil 2 nil 3 nil 9 3 1])
(función-remove-3 double? ["a" 10.2 "aa" 11.2 "b" 1 "n" "f" "lisp" "clojure" "q" ""])

;función reverse
(defn función-reverse-1
  [x]
  (reverse x))

(defn función-reverse-2
  [xs]
  (reverse xs))

(defn función-reverse-3
  [a]
  (apply str (reverse a)))

(función-reverse-1 "saul")
(función-reverse-2 [1 4 "w" \a])
(función-reverse-3 "Tecnologico Nacional de mexico")


(defn función-split-with-1
  [a b]
  (split-with a b))

(defn función-split-with-2
  [x xs]
  (split-with x xs))

(función-split-with-1 (partial >= 3) [1 2 3 4 5])
(función-split-with-2 odd? [1 3 5 6 7 9])
(función-split-with-2 (complement #{:c}) [:a :b :c :d])

(defn función-take-1
  [n xs]
  (take n xs))

(defn función-take-2
  [a b]
  (take (a b) b))

(defn función-take-3
  [a b]
  (take a b))

(función-take-1 3 '(1 2 3 4 5 6 9 10 11 12 13))
(función-take-2 first (range 2 10))
(función-take-3 3 [])

(defn función-take-while-1
  [a b]
  (take-while a b))

(defn función-take-while-2
  [xs bx]
  (take-while xs bx))

(defn función-take-while-3
  [x y]
  (take-while x y))

(función-take-while-1  neg? [-2 -5 -4 -1 0 1 2 3  21 10 11])
(función-take-while-1 neg? [0 1 2 3])
(función-take-while-2 #{[1 2] [3 4]} #{[3 4]})
(función-take-while-3 neg? nil)

(defn función-take-nth-1
  [a xs]
  (take-nth a xs))

(defn función-take-nth-2
  [a b]
  (take-nth a b))

(defn función-take-nth-3
  [f y]
  (take-nth (f y) y))

(función-take-nth-1 2 [2 3 4 5 6 7 1 11 12])
(función-take-nth-2 3 (range 20))
(función-take-nth-3 first (range 4 22 2))

(defn función-update-1
  [a b c]
  (update a b c))

(defn función-update-2
  [xs z w y]
  (update xs z w y))

(función-update-1 {:name "James" :age 26} :age inc)
(función-update-2 [1 2 3 4 5 6 7] 2 + 10)
(función-update-1 {:name "Saul" :age 26 :Peso 66} :Peso decimal?)

(defn función-update-in-1
  [p a c]
  (update-in p a c))

(defn función-update-in-2
  [p a b c]
  (update-in p a b c))

(defn función-update-3
  [a b c d e]
  (update-in a b c d e))

(función-update-in-1 {:name "James" :age 26} [:age] inc)
(función-update-in-2 {:name "James" :age 26 :altura 167 :peso 72} [:altura] + 10)
(función-update-3 {:a 3} [:a] / 4 5)